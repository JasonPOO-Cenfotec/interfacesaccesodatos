package cr.ac.ucenfotec.bl;

public class ArrayListImpl implements IAccesoDatos {
    @Override
    public String insertar(Producto producto) {
        // Aquí va la logica para almacenar en ArrayList
        return "El producto fue agregado de manera correcta en el ArrayList!";
    }

    @Override
    public String eliminar(int codigoProducto) {
        return "El producto fue eliminado en el ArrayList!";
    }
}
