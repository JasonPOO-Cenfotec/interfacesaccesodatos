package cr.ac.ucenfotec.bl;

public class CL {

    IAccesoDatos datos;

    public CL(){
        datos = new MySQLImpl();
    }

    public String agregarProducto(int codigo, String nombre){
        Producto producto = new Producto(codigo,nombre);
        return datos.insertar(producto);
    }

    public String eliminarProducto(int codigoProducto){
        return datos.eliminar(codigoProducto);
    }
}
