package cr.ac.ucenfotec.bl;

public interface IAccesoDatos {
    public abstract String insertar(Producto producto);
    public abstract String eliminar(int codigoProducto);
}
