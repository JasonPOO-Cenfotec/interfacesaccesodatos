package cr.ac.ucenfotec.bl;

public class MySQLImpl implements IAccesoDatos{
    @Override
    public String insertar(Producto producto) {
       //Aquí va la lógica para almacenar en MySQL
        return "El producto fue agregado de manera correcta en MySQL!";
    }

    @Override
    public String eliminar(int codigoProducto) {
        return "El producto fue eliminado de MySQL!";
    }
}
