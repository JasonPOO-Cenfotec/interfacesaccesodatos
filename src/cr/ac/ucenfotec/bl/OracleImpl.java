package cr.ac.ucenfotec.bl;

public class OracleImpl implements IAccesoDatos{
    @Override
    public String insertar(Producto producto) {
        // Aquí va la lógica para almacenar en BD Oracle
        return "El producto fue agregado de manera correcta en Oracle!";
    }

    @Override
    public String eliminar(int codigoProducto) {
        return "El producto fue eliminado en Oracle!";
    }
}
