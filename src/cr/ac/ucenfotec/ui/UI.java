package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.CL;

public class UI {

    static CL gestor = new CL();

    public static void main(String[] args) {

        // Insertar un producto
        String respuesta = gestor.agregarProducto(123,"Cuaderdo de resortes de 500 hojas");
        System.out.println(respuesta);

        String respuesta2 = gestor.eliminarProducto(123);
        System.out.println(respuesta2);
    }

}
